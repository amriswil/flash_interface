import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import threading
import datetime
import time
import PySimpleGUI as sg
import pygame
import subprocess
import platform
from enum import Enum, auto
from lecture_break import *
from app_config import *
from simple_thread import *
from disk import *

class FlashType(Enum):
    WAITING = auto()
    READY = auto()
    STARTING = auto()
    FLASHING = auto()

def time_thread_function(self, window):
    global time_string
    global last_break
    global next_alarm_string
    global count_down_string
    global sound_break_start
    global sound_break_end
    global shift_hour
    x = datetime.datetime.now()
    replace_hour = x.hour + shift_hour
    x = x.replace(hour = replace_hour % 24)
    time_string = x.strftime('%X')
    next_break = get_next_break(x.hour, x.minute)
    next_alarm_string = "{}:{} {}".format(next_break.hour, next_break.minute, "startet" if next_break.start else "endet")
    count_down_string = "{}:{}:{}".format(next_break.hour - x.hour, next_break.minute - x.minute - 1, 60 - x.second)
    if(next_break.hour == x.hour and next_break.minute == x.minute):
        if(next_break.start):
            sound_break_start.play()
        else:
            sound_break_end.play()
        last_break = next_break

def flash_thread_function(self, flash_id, window):
    global flash_states
    # check for flash image drive found
    flash_drive = get_flash_drive()
    if flash_drive == None:
        if flash_id == 1:
            window["status_card_1"](APPLICATION_MESSAGE_NO_FLASH_FILE)
            window["status_card_2"](APPLICATION_MESSAGE_NO_FLASH_FILE)
            flash_states[0] = FlashType.WAITING
            flash_states[1] = FlashType.WAITING
            find_and_set_flash_drive()
            return
    else:
        # check if flash devices is reserved
        reserved_drive = get_reserved_flash_drive(flash_id)
        if reserved_drive == None:
            # reserve flash drive
            disk = get_non_reserved_flash_drive()
            if disk == None:
                window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_NO_CARD_WRITER)
                flash_states[flash_id - 1] = FlashType.WAITING
                return
            disk.flash_id = flash_id
            return
        elif flash_states[flash_id - 1] != FlashType.FLASHING:
            if flash_states[flash_id - 1] == FlashType.STARTING:
                # prevent flashing on wrong system
                if platform.uname()[1] == "raspberrypi":
                    self.flash_process = subprocess.Popen(["sudo", "dd", "bs=4M", "if=" + flash_drive.image_path, "of=" + reserved_drive.device])
                    window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_STARTED_FLASH_PROCESS.format(reserved_drive.device))
                    flash_states[flash_id - 1] = FlashType.FLASHING
            elif flash_states[flash_id - 1] != FlashType.READY:
                window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_FLASH_READY.format(reserved_drive.device))
                flash_states[flash_id - 1] = FlashType.READY
        else:
            if self.flash_process == None:
                flash_states[flash_id - 1] = FlashType.READY
                window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_FLASH_ERROR.format(reserved_drive.device))
            else:
                try:
                    self.flash_process.wait(1)
                    if self.flash_process.returncode != 0:
                        flash_states[flash_id - 1] = FlashType.READY
                        window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_FLASH_ERROR.format(reserved_drive.device))
                    else:
                        flash_states[flash_id - 1] = FlashType.READY
                        self.flash_process = subprocess.Popen(["sync"])
                        self.flash_process.wait()
                        window["status_card_" + str(flash_id)](APPLICATION_MESSAGE_FLASH_DONE.format(reserved_drive.device))
                except:
                    return


def find_and_set_flash_drive():
    global disks
    result = subprocess.Popen(["mount"], stdout=subprocess.PIPE)
    mounts = list(result.stdout)
    for mount in mounts:
        for disk in disks:
            mount_string = str(mount)
            if disk.device in mount_string:
                # get path part
                path = mount_string.split(" ")[2]
                for root, dirs, files in os.walk(path, topdown=False):
                    for name in files:
                        if ".img" in name:
                            disk.image_path = os.path.join(root, name)
                            disk.type = DiskType.FLASH
                            return True
                    break
    return False

def get_flash_drive():
    global disks
    for disk in disks:
        if disk.type == DiskType.FLASH:
            if os.path.exists(disk.device):
                return disk
            else:
                return None
    return None

def get_reserved_flash_drive(flash_id):
    global disks
    for disk in disks:
        if disk.flash_id == flash_id:
            if os.path.exists(disk.device):
                return disk
            else:
                return None
    return None

def get_non_reserved_flash_drive():
    global disks
    for disk in disks:
        if disk.flash_id == None and disk.type != DiskType.IMAGE:
            if os.path.exists(disk.device):
                return disk
            else:
                return None
    return None

def get_next_break(hour, minute):
    global last_break
    for b in break_times:
        if last_break == b:
            continue
        if hour > b.hour:
            continue
        if minute > b.minute:
            continue
        return b
    return break_times[0]

pygame.mixer.init()

time_string = ""
next_alarm_string = ""
count_down_string = ""
last_break = None
sound_break_start = pygame.mixer.Sound("sounds/alarm_1.ogg")
sound_break_end = pygame.mixer.Sound("sounds/alarm_1.ogg")
shift_hour = 0

# set the disks
disks = []
for i in APPLICATION_DISK_DEVICES:
    disks.append(Disk(i))

flash_states = [FlashType.WAITING] * 2

sg.theme('DarkAmber')

col =   [[sg.Text('Flash Interface', font = 'Default 28', auto_size_text=True, size = (30, 1), justification = 'center')],
        [sg.Column([], size=(1, 22))],
        [DefaultText("Zeit", key = "time_text", size = (13, 2), justification = 'center', font = 'Default 18'), 
            DefaultText("Zeit", key = "next_alarm_text", size = (13, 2), justification = 'center', font = 'Default 18'), 
            DefaultText("Zeit", key = "count_down_text", size = (13, 2), justification = 'center', font = 'Default 18')],
        [sg.Column([], size=(1, 22))],
        [DefaultButton("Flash Card 1"), DefaultText("Status", key = "status_card_1", size = APPLICATION_STATUS_TEXT_SIZE)],
        [sg.Column([], size=(1, 2))],
        [DefaultButton("Flash Card 2"), DefaultText("Status", key = "status_card_2", size = APPLICATION_STATUS_TEXT_SIZE)],
        [sg.Column([], size=(1, 14))],
        [sg.Column([], size=(130, 1)), DefaultButton("+"), DefaultButton("-"), DefaultButton("Screen off"), DefaultButton("Update")]]

layout = [[sg.Column(col, justification='center')]]

window = sg.Window(
    APPLICATION_NAME, 
    layout, 
    no_titlebar = True, 
    location = (0, 0), 
    size = APPLICATION_WINDOW_SIZE, 
    keep_on_top = True,
    return_keyboard_events = True)

window.finalize()

time_thread = SimpleThread(target = time_thread_function, args=(window, ), repeat_delay=0.5)
flash_thread_1 = SimpleThread(target = flash_thread_function, args=(1, window), repeat_delay=0.5)
flash_thread_2 = SimpleThread(target = flash_thread_function, args=(2, window), repeat_delay=0.5)
time_thread.start()
flash_thread_1.start()
flash_thread_2.start()

while True:             
    event, values = window.read(100)
    window["time_text"]("Zeit:\n" + time_string)
    window["next_alarm_text"]("Pause:\n" + next_alarm_string)
    window["count_down_text"]("Restzeit:\n" + count_down_string)

    if flash_states[0] == FlashType.READY:
        window["Flash Card 1"].update(disabled = False)
    else:
        window["Flash Card 1"].update(disabled = True)

    if flash_states[1] == FlashType.READY:
        window["Flash Card 2"].update(disabled = False)
    else:
        window["Flash Card 2"].update(disabled = True)

    # key events
    if len(event) == 1:
        key = ord(event)
        if(key == APPLICATION_ESCAPE_KEY):
            break
    # button events
    if event in (None, 'Update'):
        subprocess.Popen(["bash", "update.sh"])
        break
    if event in (None, '+'):
        shift_hour += 1
    if event in (None, '-'):
        shift_hour -= 1
    if event in (None, 'Screen off'):
        subprocess.Popen(["xset", "-display", ":0", "dpms", "force", "off"])
    if event in (None, 'Flash Card 1'):
        flash_states[0] = FlashType.STARTING
    if event in (None, 'Flash Card 2'):
        flash_states[1] = FlashType.STARTING

time_thread.stop()
flash_thread_1.stop()
flash_thread_2.stop()
window.close()