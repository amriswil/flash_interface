from enum import Enum, auto

class Disk():
    def __init__(self, device):
        self.device = device
        self.type = DiskType.UNKNOWN
        self.image_path = None
        self.flash_id = None

class DiskType(Enum):
    UNKNOWN = auto()
    FLASH = auto()
    IMAGE = auto()