class Break():
    def __init__(self, start, hour, minute):
        self.start = start
        self.hour = hour
        self.minute = minute

break_times = [
                Break(False, 13, 45),
                Break(True, 14, 30),
                Break(False, 14, 35),
                Break(True, 15, 20),
                Break(False, 15, 35),
                Break(True, 16, 20),
                Break(False, 16, 25),
                Break(True, 16, 55),
                Break(True, 18, 2)
            ]