# Flash Interface
## Hardware
Adafruit PITFT 3.5 https://www.adafruit.com/product/2097  
Raspberry Pi 3 B+

## Setup
### System Tools
``` bash
sudo apt update
sudo apt upgrade
sudo apt install vim
```

### Git Account for automatic updates
On Gitlab go to *Settings* -> *Repository Settings* -> *Deploy Tokens* and create a token with a user name  
Now create a file `.netrc` at the home folder of the raspberry pi with the following content:
``` bash
machine gitlab.com
login TOKEN_USERNAME
password TOKEN_PASSWORD
```

### Sound Volume
``` bash
alsamixer
```
Then increase volume

### PITFT Display
``` bash
git clone https://github.com/adafruit/Raspberry-Pi-Installer-Scripts
cd Raspberry-Pi-Installer-Scripts/
sudo adafruit-pitft.sh
```

### Hiding Taskbar
``` bash
sudo vim /etc/xdg/lxsession/LXDE-pi/autostart
```
Then command out line `@lxpanel --profile LXDE` with `#@lxpanel --profile LXDE`

### Disable Removable Medium Dialog
``` bash
pcmanfm
```
In the file manager gui go to `Edit` -> `Preferences` -> `Volume Management` and deselect `Show available options...`

### Adding Cronjob
``` bash
crontab -e
```
Then add line:
``` bash
@reboot bash -c "cd /home/pi/git/flash_interface/ && DISPLAY=:0.0 python3 flash_interface.py"
```