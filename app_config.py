import PySimpleGUI as sg

APPLICATION_NAME = "Flash Interface"
APPLICATION_WINDOW_SIZE = (720, 480)

APPLICATION_ESCAPE_KEY = 27

APPLICATION_FONT = 'Default 22'
APPLICATION_BUTTON_SIZE = (5,2)
APPLICATION_STATUS_TEXT_SIZE = (26,1)

APPLICATION_DISK_DEVICES = ["/dev/sda", "/dev/sdb", "/dev/sdc"]

APPLICATION_MESSAGE_NO_FLASH_FILE = "Flash-Datei fehlt"
APPLICATION_MESSAGE_NO_CARD_WRITER = "Schreibkarte fehlt"
APPLICATION_MESSAGE_FLASH_READY = "Bereit: {}"
APPLICATION_MESSAGE_STARTED_FLASH_PROCESS = "Flashprozess gestartet: {}"
APPLICATION_MESSAGE_FLASH_ERROR = "Fehler beim Schreiben: {}"
APPLICATION_MESSAGE_FLASH_DONE = "Flashvorgang abgeschlossen: {}"

class DefaultButton(sg.Button):
    def __init__(self, button_text=''):
        super().__init__(button_text = button_text, size = APPLICATION_BUTTON_SIZE, font = APPLICATION_FONT)

class DefaultText(sg.Text):
    def __init__(self, text = '', key = '', size = (None, None), justification = None, font = APPLICATION_FONT):
        super().__init__(text = text, font = font, auto_size_text = True, key = key, size = size, justification = justification)