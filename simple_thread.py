import threading
import time

class SimpleThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, repeat_delay=0):
        super().__init__(group, self._run_thread_function, name, args, kwargs, daemon=None)
        self._running = False
        self._thread_repeated_function = target
        self._repeat_delay = repeat_delay

    def _run_thread_function(self, *args):
        while self._running:
            self._thread_repeated_function(self, *args)
            time.sleep(self._repeat_delay)
    
    def start(self):
        self._running = True
        return super().start()

    def stop(self):
        self._running = False
        self.join()